package main


import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestExecGitCommand(t *testing.T) {
	expected := `origin	git@bitbucket.org:momirov/go-bitbucket.git (fetch)
origin	git@bitbucket.org:momirov/go-bitbucket.git (push)
`
	origin := ExecGitCommand()
	assert.Equal(t, expected, origin)
}

func TestGetOrigin(t *testing.T) {
	assert.Equal(t, "momirov/go-bitbucket", GetOrigin())
}

func TestParseOrigin(t *testing.T) {
	origin := `origin	git@bitbucket.org:momirov/go-bitbucket.git (fetch)
origin	git@bitbucket.org:momirov/go-bitbucket.git (push)
`
	assert.Equal(t, "momirov/go-bitbucket", ParseOrigin(origin))
}

func TestMakeUrl(t *testing.T) {
	assert.Equal(t, "https://api.bitbucket.org/2.0/repositories/some/repo/pullrequests/", MakeUrl("some/repo"))
}

func TestMakeJson(t *testing.T) {
	assert.Equal(t, "{\"title\":\"Test request\",\"source\":{\"branch\":{\"name\":\"test\"}}}", string(MakeJson()))
}
