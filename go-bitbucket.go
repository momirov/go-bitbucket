package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"os/exec"
	"regexp"
	"strings"
)

type PullRequest struct {
	Title  string `json:"title"`
	Source Source `json:"source"`
}

type Source struct {
	Branch Branch `json:"branch"`
}

type Branch struct {
	Name string `json:"name"`
}

type Repository struct {
	FullName string `json:"full_name"`
}

func ExecGitCommand() string {
	origin, err := exec.Command("git", "remote", "-v").Output()
	if err != nil {
		log.Fatal(err)
	}
	return string(origin)
}

func ParseOrigin(s string) string {

	re, err := regexp.Compile(`(?m)^origin.+git@bitbucket\.org:(?P<id>.*)\.git.*$`)
	if err != nil {
		log.Fatal(err)
	}
	res := re.FindStringSubmatch(string(s))[1]

	return res
}

func GetOrigin() string {
	return ParseOrigin(ExecGitCommand())
}

func GetCredentials() (string, string) {
	var username string
	fmt.Print("Username: ")
	fmt.Scanf("%s", &username)
	var password string
	fmt.Print("Password: ")
	fmt.Scanf("%s", &password)
	return username, password
}

func MakeUrl(repo string) string {
	return "https://api.bitbucket.org/2.0/repositories/" + repo + "/pullrequests/"
}

func MakeJson() string {
	m := PullRequest{"Test request", Source{Branch{"test"}}}

	b, err := json.Marshal(m)
	if err != nil {
		log.Fatal(err)
	}
	return string(b)
}

func main() {
	username, password := GetCredentials()
	json := MakeJson()
	s, err := exec.Command("git", "remote", "-v").Output()
	if err != nil {
		log.Fatal(err)
	}
	ress := ParseOrigin(string(s))
	url := MakeUrl(ress)

	req, err := http.NewRequest("POST", url, strings.NewReader(json))
	req.Header.Add("Content-Type", "application/json")
	req.SetBasicAuth(username, password)

	by, err := httputil.DumpRequest(req, true)
	fmt.Printf("%s", by)
	if err != nil {
		log.Fatal(err)
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	responseString, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s", responseString)
}
